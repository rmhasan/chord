
# Takes in args as an array, returns a dict 
# console:true/false
# fileName: $filename
# numPeers: $NumPeers



def parseArgs(argsIn):
	argsOut = {}
	# Start with default settings
	argsOut['console'] = True
	argsOut['fileName'] = 'datetime'
	argsOut['numPeers'] = 5
	i = 0
	while i < len(argsIn):
		if argsIn[i] == '-c':
			i = i + 1
			if i >= len(argsIn):
				print '[FATAL] Incorrect args usage'
				sys.exit(0)
			if argsIn[i] in ['t', 'T', 'true', 'True', 'TRUE']:
				argsOut['console'] = 'True'
			elif argsIn[i] in ['f', 'F', 'false', 'False', 'FALSE']:
				argsOut['console'] = 'False'
			else:
				print '[FATAL] Incorrect console option. Only true/false accepted'
				sys.exit(0)
		elif argsIn[i] == '-f':
			i = i + 1
			if i >= len(argsIn):
				print '[FATAL] Incorrect args usage'
				sys.exit(0)
			argsOut['fileName'] = argsIn[i]
		elif argsIn[i] == '-p':
			i = i + 1
			if i >= len(argsIn):
				print '[FATAL] Incorrect args usage'
				sys.exit(0)
			try:
				argsOut['numPeers'] = int(argsIn[i])
			except ValueError:
				print '[FATAL] Number of peers needs to be passes in as an integer'
				sys.exit(0)
		else:
			print '[FATAL] Unrecognized arg: %s' % (argsIn[i])
			sys.exit(0)
		i = i + 1
	return argsOut
		