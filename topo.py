import inspect
import os
from mininext.topo import Topo
from mininext.services.quagga import QuaggaService

from collections import namedtuple

SWITCH_NAME = 's0'

class QuaggaTopo(Topo):
    def __init__(self, numHosts):
        Topo.__init__(self)
        self.addSwitch(SWITCH_NAME)
        self.topoHosts = []
        for i in range(1,numHosts+1):
            host = 'h%d'%i
            hostip = '192.168.1.%d'%(i)
            self.topoHosts.append((host,hostip))
            self.addHost(host,ip=hostip+'/23')
            self.addLink(host,SWITCH_NAME)
