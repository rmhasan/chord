import sys, os,hashlib
from misc import *
from peer import Peer
from threading import Thread
from simplelogger import Logger

if len(sys.argv) != 4:
    print 'usage: spawnpeer.py <host ip> <contact ip> <peer id>'
    sys.exit(0)

pid = os.fork()
if pid != 0:
    sys.exit(0)

port = 6000
logger = Logger('Output/%s.log'%sys.argv[1].replace('.','_'))
peerId = int(sys.argv[3])
hostIp = sys.argv[1]
peer = Peer(peerId,hostIp,port,logger)
t = Thread(target=peer.runServer,args=())
t.start()
if sys.argv[2] != '0':
    send('Join\n%s\n%d\n%s'%(hostIp,port,hashlib.sha1(str(peerId)).hexdigest()),sys.argv[2],6000)
t.join()
