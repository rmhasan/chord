import os, sys, time,hashlib,thread,socket
from simplelogger import Logger
from peer import Peer
from random import randint

PEERID_MULTIPLE = 10
PEER_LIMIT = 10

def send(msg,host,port,client=None):
    msgLen = len(msg)
    totalSent = 0
    if client == None:
        sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        sock.connect((host,port))
        sock.settimeout(10)
    else:
        sock = client
    while totalSent < msgLen:
        sent = sock.send(msg[totalSent:])
        if sent == 0:
            raise RuntimeError('Socket connection broken')
        totalSent += sent
    if client == None:
        sock.close()

def pushData(peers,sid):
    keys = []
    n = len(peers)-1
    for i in range(sid,sid+PEERID_MULTIPLE*PEER_LIMIT):
        peer = peers[randint(0,n)]
        key = hashlib.sha1(str(i)).hexdigest()
        msg = 'store\n%s\n%d'%(hashlib.sha1(str(i)).hexdigest(),i)
        send(msg,peer.host,peer.serverPort)
        keys.append(key)
    return keys
                                                            

if len(sys.argv) != 4:
    print 'usage: mkswarm.py <host ip> <contact ip> <starting peerid>'
    sys.exit(0)

pid = os.fork()
if pid != 0:
    sys.exit(0)
        
logger = Logger('Output/%s.log'%sys.argv[1].replace('.','_'))
peerId = int(sys.argv[3])
hostIp = sys.argv[1]
contactIp = sys.argv[2] if sys.argv[2] != '0' else hostIp
port = 6000

firstPeer = Peer(peerId,hostIp,port,logger)
thread.start_new_thread(firstPeer.runServer,())
newPeers = []
time.sleep(2)
for i in range(PEER_LIMIT-1):
    port += 1
    peerId += 10
    newPeer = Peer(peerId,hostIp,port,logger)
    thread.start_new_thread(newPeer.runServer,())
    send('Join\n%s\n%d\n%s'%(hostIp,port,hashlib.sha1(str(peerId)).hexdigest()),contactIp,6000)
    newPeers.append(newPeer)
    time.sleep(3)

time.sleep(3)
pushData(newPeers,firstPeer.peerId)
time.sleep(3)
logger.info(-1,'This swarm is ready')

while True:
    pass
