import sys, hashlib
from misc import *

def pushData(ipAddr,limit):
    for i in range(limit+1):
        key = hashlib.sha1(str(i)).hexdigest()
        msg = 'store\n%s\n%d'%(hashlib.sha1(str(i)).hexdigest(),i)
        send(msg,ipAddr,6000)

if len(sys.argv) != 3:
    print 'usage: pushdata.py <ip address> <upper limit>'
    sys.exit(0)

pushData(sys.argv[1],int(sys.argv[2]))
