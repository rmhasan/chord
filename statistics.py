# Statistics Framework for CHORD project
# Reads in a set of pickle files specified in the input,
# Loads their data, generates meaningful statistics and plots
# from it sorted by run type

# Install directions for matplotlib/numpy
# either 'pip install matplotlib'/'pip install numpy' [Linux]
# or 'python -m pip install ###' [Windows]

# For now, this just examines aggregate statistics for a collection of runs, not the data from each individual run

import sys
import cPickle as pickle
import matplotlib.pyplot as plt
import numpy as np
from chordtest import TestInstance # Not sure we actually need this here

def main():
	if len(sys.argv) <= 1:
		print '[ERROR] Need to pass in at least one pickle file'
		return
		
	if not os.path.exists('Plots'):
			os.makedirs('Plots')
		
	filesToAnalyze = []
	for file in sys.argv[1:]:
		inFile = open(file, 'rb')
		if inFile == None:
			print '[WARN] Unable to open input file: %s' % (inFile)
			continue
		ti = pickle.load(inFile)
		close(inFile)
		fileToAnalyze.append(ti)
		
	# Now we sort these, whichever ones have the same filename will be examined together
	runData = {}
	for file in filesToAnalyze:
		if file.name not in runData.keys():
			runData[file.name] = {} # NumPeers values are the keys here
		if file.params['NumPeers'] not in runData[file.name].keys():
			runData[file.name][file.params['NumPeers']] = []
		runData[file.name][file.params['NumPeers']].append(file)
	
	
	# The only other parameters are Direction and Initialized
	for runType, data in runData.items():
		# These are the four varieties that we can have per run type (where 'swarm' is one type)
		xValuesBi = []
		yValuesBi = []
		xValuesUni = []
		yValuesUni = []
		xValuesInitBi = []
		yValuesInitBi = []
		xValuesInitUni = []
		yValuesInitUni = []
		# Now we "slice" the data according to each parameter (x-axis is NumPeers)
		for peerCount, runs in data.items():
			for run in runs:
				if run.params['Direction'] == 'Bi' and run.params['Initialized'] == 'False':
					xValuesBi.append(peerCount)
					yValuesBi.append(np.average(run.times))
				if run.params['Direction'] == 'Uni' and run.params['Initialized'] == 'False':
					xValuesUni.append(peerCount)
					yValuesBi.append(np.average(run.times))
				if run.params['Direction'] == 'Bi' and run.params['Initialized'] == 'True':
					xValuesInitBi.append(peerCount)
					yValuesInitBi.append(np.average(run.times))
				if run.params['Direction'] == 'Uni' and run.params['Initialized'] == 'True':
					xValuesInitUni.append(peerCount)
					yValuesInitUni.append(np.average(run.times))
		# And finally plot this, and save it
		fig, ax = plt.subplots( nrows=1, ncols=1 )
		ax.plot(xValuesBi, yValuesBi, 'r--',
				xValuesUni, yValuesUni, 'g*',
				xValuesInitBi, yValuesInitBi, 'b^',
				xValuesInitUni, yValuesInitUni, 'ms')
		ax.show()
		fig.savefig('Plots/%s.png' % (runType))
		plt.close(fig)
		
	# For now, we are not doing an analysis of which variants are faster. That will come once there is REAL data
	



if __name__ == '__main__':
	main()