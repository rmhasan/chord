# CSV parser/analyzer
# Takes in as parameters a set of csv files (rel-path), loads their data,
# performs an analysis, and generates relevant plots
# Generates: 
#
#		Line plot of the average time for each of the 20 iterations (different series per csv file) [with error bars]
#		Average of the sum total of data as a bar graph
#		CDF of individual search times (all iterations per file clustered together)
#		PDF plot of individual search times (all iterations per file clustered together)
#
# 	Format for each csv file should be:
# Line 1: series name
# Line 2-n: data, data, ... (should always be same number of columns per row)

import gc
import os
import sys
import time
import matplotlib.pyplot as plt
import numpy as np
import cPickle as pickle
from logger import Logger

MeshGranularity = 0.001


class ColorWheel:

	def __init__(self):
		self.markers = ['r-', 'g-', 'b-', 'm-', 'k-']
		self.curr = -1

	def next(self):
		self.curr += 1
		if self.curr > len(self.markers):
			self.curr = 1
		return self.markers[self.curr]


class PlotDataSeries:

	def __init__(self, name, means, stdevs, xMesh, cdfMesh, pdfMesh):
		self.name = name
		self.means = means
		self.stdevs = stdevs
		self.xMesh = xMesh
		self.cdfMesh = cdfMesh
		self.pdfMesh = pdfMesh


class DataSeries:

	def __init__(self, data, seriesName):
		self.data = data
		self.name = seriesName[:-1]
		self.mean = None
		self.stdev = None
		self.transpose() # performs transpose matrix op on the data, assumes it is square
		self.subSeriesMeans = []
		self.subSeriesStdevs = []
		self.sortedData = self.sortData()
		self.min = self.sortedData[0]
		self.max = self.sortedData[-1]
		self.count = len(self.sortedData)
		self.xMesh = self.generateMesh(self.min, self.max, MeshGranularity)
		print 'Min: %s\tMax: %s\tCount: %s\tXMeshSize: %s' % (self.min, self.max, self.count, len(self.xMesh))
		self.cdfMesh = self.generateCDFMesh()
		self.pdfMesh = self.generatePDFMesh(MeshGranularity)
		self.analyze()
		print 'Name: %s\tMean: %s\tStdev: %s\tCDFMeshSize: %s\tPDFMeshSize: %s' % (self.name, self.mean, self.stdev, len(self.cdfMesh), len(self.pdfMesh))
		self.generateSelfPlots()
		
	def transpose(self):
		# Mutates self.data, does transpose operation and converts to int
		newData = []
		i = 0
		while i < len(self.data[0]):
			newSubSeries = []
			for row in self.data:
				newSubSeries.append(float(row[i]))
			newData.append(newSubSeries)
			i += 1
		self.data = newData
	
	def sortData(self):
		# Flattens and sorts data
		sortedData = []
		for row in self.data:
			for item in row:
				sortedData.append(item)
		sortedData.sort()
		return sortedData
			
	def generateMesh(self, min, max, step):
		mesh = []
		curr = min
		mesh.append(min)
		while curr < max:
			curr += step
			mesh.append(curr)
		if curr != max:
			mesh.append(max)
		return mesh

	def generateCDFMesh(self):
		cdfMesh = []
		currIndex = 0
		for step in self.xMesh:
			if currIndex < len(self.sortedData):
				while step > self.sortedData[currIndex]:
					# print 'Step: %s, currIndex: %s, sortedData[currIndex]:%s, step > ...: %s' % (step, currIndex, self.sortedData[currIndex], (step >= self.sortedData[currIndex]))
					currIndex += 1
					if currIndex >= len(self.sortedData):
						break
			# print 'Appending CDFMesh %s' % (float(currIndex)/float(self.count))
			cdfMesh.append(float(currIndex)/float(self.count))
			
		return cdfMesh
		
		
	def generatePDFMesh(self, step):
		# Discretized PDF mesh, granularity dependent on resolution of data
		cdfDeltaPoints = [] # Set of indices where cdf value changes, left-aligned
		prevPoint = 0
		prevPointIndex = 0
		cdfDeltaPoints.append(0)
		i = 0
		for point in self.cdfMesh:
			if point != prevPoint:
				prevPoint = point
				prevPointIndex = i
				cdfDeltaPoints.append(i)
			i += 1
		# Now we do a discrete differentiation over the cdf to obtain a rough pdf
		pdfMesh = []
		pdfMesh.append(0)
		i = 1
		while i < len(cdfDeltaPoints):
			slope = (self.cdfMesh[cdfDeltaPoints[i]] - self.cdfMesh[cdfDeltaPoints[i-1]])/(step*(cdfDeltaPoints[i] - cdfDeltaPoints[i-1]))
			j = 0
			while j < (cdfDeltaPoints[i] - cdfDeltaPoints[i-1]):
				pdfMesh.append(slope)
				j += 1
			i += 1
		pdfMesh.append(0)
		return pdfMesh
		
	def analyze(self):
		self.mean = np.average(self.sortedData)
		self.stdev = np.std(self.sortedData)
		for row in self.data:
			self.subSeriesMeans.append(np.average(row))
			self.subSeriesStdevs.append(np.std(row))

	def generatePlot(self, xData, yData, plotName, icon):
		fig, ax = plt.subplots( nrows=1, ncols=1 )
		iconToUse = 'r-'
		if icon != '':
			iconToUse = icon
		ax.plot(xData, yData, iconToUse)
		# ax.show()
		filename = '%s_%s.png' % (self.name, plotName)
		plt.figsize=(60.0, 60.0)
		fig.savefig(os.path.join('Plots', filename), dpi=1000)
		plt.close(fig)
			
	def generateSelfPlots(self):
		self.generatePlot(range(len(self.subSeriesMeans)), self.subSeriesMeans, "MeanByIteration", 'r-*')
		self.generatePlot(range(len(self.subSeriesStdevs)), self.subSeriesStdevs, "StdevByIteration", 'r-*')
		self.generatePlot(self.xMesh, self.cdfMesh, "CDF", '')
		self.generatePlot(self.xMesh, self.pdfMesh, "PDF", '')
		

def extractAttr(pickleFileName, attr):
	print 'Extracting %s from %s' % (attr, pickleFileName)
	pickleFile = open(pickleFileName, 'rb')
	if pickleFile == None:
		print 'Failed to open pickle file'
		sys.exit(1)
	ds = pickle.load(pickleFile)
	pickleFile.close()
	return getattr(ds, attr)
			
def main():
	logArgs = {'level':'DEBUG', 'timestamp':'True'}
	log = Logger(**logArgs)
	if len(sys.argv) < 2:
		log.fatal(-1, "Need to pass in at least ONE csv file")
		return
	
	if not os.path.exists('Plots'):
			os.makedirs('Plots')
	if not os.path.exists('Analysis'):
			os.makedirs('Analysis')
		
	
	# Open and read every csv file specified
	args = sys.argv[1:]
	filesRead = {}
	for arg in args:
		handle = open(arg)
		lines = handle.readlines()
		filesRead[lines[0]] = []
		for line in lines[1:]:
			filesRead[lines[0]].append(line.split(', '))
		handle.close()
	# Much of the analysis is implicit in the DataSeries constructor
	allData = []
	plotData = []
	for key, value in filesRead.items():
		# Need to pickle these dataSeries objects to preserve memory...
		# Spawn a new Python process per "slice"
		gc.collect()
		ds = DataSeries(value, key)
		dsFileName = '%s.pickle' % (ds.name)
		dsFileName = os.path.join('Analysis', dsFileName)
		dsFile = open(dsFileName, 'wb')
		pickle.dump(ds, dsFile)
		# We only store the pickle filename for memory conservation
		allData.append(dsFileName)
		plotData.append(PlotDataSeries(ds.name, ds.subSeriesMeans, ds.subSeriesStdevs, ds.xMesh, ds.cdfMesh, ds.pdfMesh))
	# time.sleep(5)
	# Now we generate comparative plots {mean, stdev, cdf, pdf}
	gc.collect()
	meanSets = {}
	stdevSets = {}
	for pd in plotData:
		meanSets[pd.name] = pd.means
		stdevSets[pd.name] = pd.stdevs
		# meanSets.append(extractAttr(ds, 'subSeriesMeans'))
		# stdevSets.append(extractAttr(ds, 'subSeriesStdevs'))
	fig = plt.figure()
	ax = fig.add_subplot(111)
	cw = ColorWheel()
	for name, set in meanSets.items():
		# Need a marker/color wheel
		ax.plot(range(len(set)), set, cw.next(), label=name)
	plt.title('Mean search times by iteration by variation')
	plt.legend(loc='upper right')
	# plt.show()
	fig.savefig(os.path.join('Plots', 'meanByIteration.png'), dpi=1000)
	plt.close(fig)
	fig = plt.figure()
	ax = fig.add_subplot(111)
	cw = ColorWheel()
	for name, set in stdevSets.items():
		# Need a marker/color wheel
		ax.plot(range(len(set)), set, cw.next(), label=name)
	plt.title('Search time Standard Deviation by iteration by variation')
	plt.legend(loc='upper right')
	# plt.show()
	fig.savefig(os.path.join('Plots', 'stdevByIteration.png'), dpi=1000)
	plt.close(fig)
	gc.collect()
	xMeshSets = {}
	cdfSets = {}
	pdfSets = {}
	for pd in plotData:
		xMeshSets[pd.name] = pd.xMesh
		cdfSets[pd.name] = pd.cdfMesh
		pdfSets[pd.name] = pd.pdfMesh
	fig = plt.figure()
	ax = fig.add_subplot(111)
	cw = ColorWheel()
	for name, set in xMeshSets.items():
		# Need a marker/color wheel
		ax.plot(set, cdfSets[name], cw.next(), label=name)
	plt.title('Cumulative Distribution Function of Search Times by variation')
	plt.legend(loc='lower right')
	x1,x2,y1,y2 = plt.axis()
	plt.axis((x1,0.3,y1,y2))
	# plt.show()
	fig.savefig(os.path.join('Plots', 'cdf.png'), dpi=1000)
	plt.close(fig)
	fig = plt.figure()
	ax = fig.add_subplot(111)
	cw = ColorWheel()
	for name, set in xMeshSets.items():
		# Need a marker/color wheel
		ax.plot(set, pdfSets[name], cw.next(), label=name)
	plt.title('Probability Distribution Function of Search Times by variation')
	plt.legend(loc='upper right')
	x1,x2,y1,y2 = plt.axis()
	plt.axis((x1,0.3,y1,y2))
	# plt.show()
	fig.savefig(os.path.join('Plots', 'pdf.png'), dpi=1000)
	plt.close(fig)
	gc.collect()
	
	
if __name__ == '__main__':
	main()