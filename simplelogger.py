import threading, datetime

class Logger:
    def __init__(self, filename):
        #self.fileObj = open(filename,'w+',0)
        self.lock = threading.Lock()

    def debug(self,peerId,msg):
        self.write('DEBUG',peerId,msg)

    def error(self,peerId,msg):
        self.write('ERROR',peerId,msg)

    def warn(self,peerId,msg):
        self.write('WARN',peerId,msg)

    def fatal(self,peerId,msg):
        self.write('FATAL',peerId,msg)

    def info(self,peerId,msg):
        self.write('INFO',peerId,msg)

    def write(self,level,peerId,msg):
        dt = datetime.datetime.now()
        out = '[%s] {%s} (peer %d) %s\n'%(level,dt.strftime('%Y/%m/%d %H:%M:%S'),peerId,msg)
        #with self.lock:
            #self.fileObj.write(out)
            #print out
