from threading import Lock

class FingerTable:
    def __init__(self,key,limit=200):
        self.limit = limit
        self.key = key
        self.successorFingers = {}
        self.predecessorFingers = {}
        self.lock = Lock()

    def addNewEntry(self,entry):
        self.lock.acquire()
        if entry[0] in self.successorFingers:
            self.successorFingers[entry[0]] = entry
        elif entry[0] in self.predecessorFingers:
            self.predecessorFingers[entry[0]] = entry
        elif entry[0] >= self.key:
            if len(self.successorFingers) > self.limit:
                self.successorFingers.pop(self.successorFingers.keys()[0])
            self.successorFingers[entry[0]] = entry
        else:
            if len(self.predecessorFingers) > self.limit:
                self.predecessorFingers.pop(self.predecessorFingers.keys()[0])
            self.predecessorFingers[entry[0]] = entry
        self.lock.release()

    def getSuccessors(self,entry):
        successors = []
        succKeys = []
        self.lock.acquire()
        for tup in self.successorFingers.iteritems():
            if tup[1][0] > entry[0]:
                if not tup[1][0] in succKeys:
                    successors.append(tup[1])
                    succKeys.append(tup[1][0])
        self.lock.release()
        return successors
     
    def getPredecessors(self,entry):
        predecessors = []
        predKeys = []
        self.lock.acquire()
        for tup in self.predecessorFingers.iteritems():
            if tup[1][0] < entry[0]:
                if not tup[1][0] in predKeys:
                    predecessors.append(tup[1])
                    predKeys.append(tup[1][0])
        self.lock.release()
        return predecessors
