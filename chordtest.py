# ChordTest.py
# This file reads an xml file, executes tests, times them, and stores the data as a 
# pickle binary, ready to be accessed later for processing/statistics
#
# As far as the logging is concerned for the individual tests, we want it suppressed
# so as to not interfere with the timing of the execution


from args import *
from logger import Logger
import xml.etree.ElementTree as etree
import cPickle as pickle
import datetime as dt

class TestInstance:
	
	# Name should be a string
	# Params should be an array. Eg: # of Peers, Reversible?, ...
	# Needs formalizing
	def __init__(self, name)
		self.name = name
		self.times = [] # Array of run times
		self.params = {} # List of parameters that apply to this particular test
		self.params['Direction'] = 'Bi'
		self.params['Initialized'] = 'False'
		
	def setParam(self, param, value):
		self.params[params] = value
		
	def recordTime(self, time):
		self.times.append(time)
		
	def toPickle(self, outFileName):
		outFile = open(outFileName, 'wb')
		pickle.dump(self, outFile)
		close(outFile)


def main():
	"""
		This takes in a single argument for now, the path/name to an xml file that specifies which tests are to
		be done. As far as creating the Logger is concerned, going with default settings
	"""
	
	defaultArgs = parseArgs([])
	logArgs = {'outFile':args['fileName'], 'outDir':'Output', 'level':'DEBUG', 'timestamp':'True', 'console':args['console']}
	log = Logger(**logArgs)
	
	args = sys.argv[1:]
	if len(args) < 1:
		log.fatal(-2, "No args passed to ChordTest. Expect a single arg: name of XML test file to run")
		return
	
	root = etree.parse(args[0]).getroot()
	if root.tag != 'ChordTest':
		log.warn(-2, "Why aren't you naming the XML file ChordTest? Breaking convention is a no-no")
		
	for child in root:
		attributes = child.attrib
		if 'name' not in attributes.keys() or 'runs' not in attributes.keys() or 'outfile' not in attributes.keys():
			log.error(-2, "Improper ChordTest XML specification")
			continue
		
		ti = TestInstance(attributes['name'])
		# Go through all miscellaneous parameters, add them to our test instance
		numPeers = 10
		biDirectional = True
		initialized = False
		if 'NumPeers' in attributes.keys():
			ti.setParam('NumPeers', attributes['NumPeers'])
			numPeers = attributes['NumPeers']
		if 'Direction' in attributes.keys():
			ti.setParam('Direction', attributes['Direction'])
			if attributes['Direction'] == 'Uni':
				biDirectional = False
		if 'Initialized' in attributes.keys():
			ti.setParam('Initialized', attributes['Initialized'])
			initialized = attributes['Initialized']
		# Identify the test we want, call that files .test() method -- we don't want to call it as an executable. 
		# That factors the loader into the time measuring
		testObject = None
		if ti.name == 'swarm':
			testObject = Swarm(numPeers, biDirectional, initialized)
		elif ti.name == 'roundrobin':
			testObject = RoundRobin(numPeers, biDirectional, initialzed)
		else:
			log.error(-2, "Improper test name specified: %s\t\tExpect one of: {swarm, roundrobin}")
			continue
		numRuns = int(attributes['runs'])
		for run in numRuns:
			start = dt.datetime.now()
			testObject.test()
			end = dt.datetime.now()
			ti.recordTime((end - start).total_seconds())
		
		ti.toPickle(attributes['outfile'])

if __name__ == '__main__':
	main()