import socket, time, sys, hashlib
from random import randint
from misc import *

def searchData(peerIp,hostIp,keys,ttl):
    execTimes = []
    for key in keys:
        try:
            send('%s\n%s\n%d\n%s\n%d'%('search',key,ttl,hostIp,49999),peerIp,6000)
            server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            server.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
            server.bind(('',49999))
            server.listen(1)
            t1 = time.clock()
            client, addr = server.accept()
            reply = recv(client)
            reply = reply.strip()
            repComps = reply.split('\n')
            if repComps[0] == 'FoundKey':
                print 'Found Key %s at %s:%s data: %s'%(key,repComps[2],repComps[3],'\n'.join(repComps[4:]))
                execTimes.append((time.clock() - t1)*1000)
                send('storeFinger\n%s\n%s\n%s'%(repComps[1],repComps[2],repComps[3]),peerIp,6000)
            else:
                execTimes.append(-1)
                print reply
            server.close()
        except Exception as ex:
            print ex
    return execTimes


if len(sys.argv) != 5:
    print 'usage search.py <peer ip> <host ip> <upper limit> <ttl>'
    sys.exit(0)

keys = []
numKeys = int(sys.argv[3])
ttl = int(sys.argv[4])
for i in range(numKeys):
    keys.append(hashlib.sha1(str(i)).hexdigest())

csvOut = []
for i in range(2):
    csvOut.append(searchData(sys.argv[1],sys.argv[2],keys,ttl))

resOut = open('results/results_%d.csv'%numKeys,'w+')
resOut.write('Sending, %d, search, requests, to, %s\n'%(numKeys, sys.argv[1]))
for iteration in range(numKeys):
    s = ''
    for i in range(1):
        s += str(csvOut[i][iteration])+', '
    s += str(csvOut[1][iteration])
    resOut.write(s+'\n')
