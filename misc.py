import socket

def send(msg,host,port,client=None):
    msgLen = len(msg)
    totalSent = 0
    if client == None:
        sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        sock.connect((host,port))
        sock.settimeout(10)
    else:
        sock = client
    while totalSent < msgLen:
        sent = sock.send(msg[totalSent:])
        if sent == 0:
            raise RuntimeError('Socket Connection Broken')
        totalSent += sent
    if client == None:
        sock.close()


def recv(client):
    msg = ''
    client.settimeout(10)
    while True:
        data = client.recv(1024)
        if not data:
            break
        msg += (''+data)
    return msg.strip()
