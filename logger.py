import datetime
import threading
import os, sys


#   kwargs:
#   
#   	outFile -- filename to write to (otherwise, prints to console)
#   				if 'datetime' is the argument value, then a filename is generated
#   				based on the current datetime
#		outDir -- output directory (create if not exists)
#   	level -- level to print to (INFO, FATAL, WARN, ERROR, DEBUG)
#   				Choosing 'error', for example, also includes 'info', 'fatal', and 'warn'
#   	timestamp -- true/false
#   	console -- true/false


class Logger:

	def __init__(self, **kwargs):
		self.lock = threading.Lock()
		self.out = None
		self.hasLogFile = False
		self.level = 1 # This corresponds to INFO. 2 is FATAL. 3 is WARN. 4 is ERROR. 5 is DEBUG
		self.timestamp = True # By default, print a timestamp
		self.console = True # By default, print output to console
		self.outDir = 'Output'
		if 'outDir' in kwargs.keys():
			self.outDir = kwargs['outDir']
		if not os.path.exists(self.outDir):
			os.makedirs(self.outDir)
		if 'outFile' in kwargs.keys():
			self.hasLogFile = True
			if kwargs['outFile'] == 'datetime':
				d = datetime.datetime.now()
				fileName = d.strftime('%Y%m%d%H%M%S.log') #Format in purely chronological order
				self.out = open(os.path.join(self.outDir, fileName), 'w')
			else:
				self.out = open(os.path.join(self.outDir, kwargs['outFile']),'w')
		if 'level' in kwargs.keys():
			if kwargs['level'] in ['NULL', 'null', 'n', 'N']:
				self.level = 0
			if kwargs['level'] in ['INFO', 'info', 'i', 'I']:
				self.level = 1
			elif kwargs['level'] in ['FATAL', 'fatal', 'f', 'F']:
				self.level = 2
			elif kwargs['level'] in ['WARN', 'warn', 'w', 'W']:
				self.level = 3
			elif kwargs['level'] in ['ERROR', 'error', 'e', 'E']:
				self.level = 4
			elif kwargs['level'] in ['DEBUG', 'debug', 'd', 'D']:
				self.level = 5
		if 'timestamp' in kwargs.keys():
			if kwargs['timestamp'] in ['t', 'true', 'T', 'True', 'TRUE']:
				self.timestamp = True
			elif kwargs['timestamp'] in ['f', 'false', 'F', 'False', 'FALSE']:
				self.timestamp = False
		if 'console' in kwargs.keys():
			if kwargs['console'] in ['t', 'true', 'T', 'True', 'TRUE']:
				self.console = True
			elif kwargs['console'] in ['f', 'false', 'F', 'False', 'FALSE']:
				self.console = False
		# Let's add a log line for registering the logger
		self.log(-1, 5, "Logger Registered at %s level. Console Printing:%s" % (self.getLevel(self.level), self.console))
		return
		
	def getLevel(self, msgLevel):
		if msgLevel == 1:
			return 'INFO'
		elif msgLevel == 2:
			return 'FATAL'
		elif msgLevel == 3:
			return 'WARN'
		elif msgLevel == 4:
			return 'ERROR'
		else:
			return 'DEBUG'
		
	def debug(self, peerId, msg):
		self.log(peerId, 5, msg)
		return
		
	def error(self, peerId, msg):
		self.log(peerId, 4, msg)
		return
		
	def warn(self, peerId, msg):
		self.log(peerId, 3, msg)
		return
		
	def fatal(self, peerId, msg):
		self.log(peerId, 2, msg)
		return
		
	def info(self, peerId, msg):
		self.log(peerId, 1, msg)
		return
		
	def log(self, peerId, lvl, msg):
		msgLevel = lvl
		# Bounding level of msg to [1, ..., 5]
		if lvl > 5:
			msgLevel = 5
		elif lvl < 1:
			msgLevel = 1
		# Ignore this msg if at a higher level than desired
		if msgLevel > self.level:
			return
		ts = ''
		if self.timestamp:
			d = datetime.datetime.now()
			ts = d.strftime('%Y/%m/%d %H:%M:%S') # Changed to chronological order YYYYMMDDHHMMSS (easier for sorting)
		newLine = ''
		if msg[-1] != '\n':
			newLine = '\n'
		# Now build up our logging message
		message = '[%s] {%s} (peer:%s) %s%s' % (self.getLevel(msgLevel), ts, peerId, msg, newLine)
		self.lock.acquire()
		if self.console:
			print message
		if self.hasLogFile:
			self.out.write(message)
		self.lock.release()
		return
        
