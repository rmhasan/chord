import socket, hashlib, time, traceback, sys
from fingertable import FingerTable
from threading import Thread
from threading import Lock

freePorts = [20000 + x for x in range(10000)]
portsLock = Lock()

class Peer:
    def __init__(self,peerId,host,sport,logger):
        self.key = str(hashlib.sha1(str(peerId)).hexdigest())
        self.host = host
        self.peerId = peerId
        self.serverPort = sport
        self.server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.successor = (self.key,self.host,self.serverPort)
        self.predecessor = (self.key,self.host,self.serverPort)
        self.log = logger
        self.values = {}
        self.fingerTable = FingerTable(self.key)
        self.lock = Lock()
        self.handlers = {'Join':self.joinHandler,
                         'addPeer':self.addPeerHandler,
                         'setSuccessor':self.setSuccessorHandler,
                         'setPredecessor':self.setPredecessorHandler,
                         'store': self.storeHandler,
                         'topology': self.topologyMapperHandler,
                         'search': self.searchHandler,
                         'fingerSearch': self.fingerSearchHandler,
                         'storeFinger': self.storeFingerHandler,
                         'forwardSearch':self.forwardSearchHandler,
                         'reverseSearch':self.reverseSearchHandler}
        if not self.serverBind():
            self.log.fatal(self.peerId,'Could not bind sockets to port numbers')
            raise Exception()
        
    def joinHandler(self,msgLines,client):
        newMsgLines = ['addPeer', msgLines[1], msgLines[2], msgLines[3], self.host, str(self.serverPort)]
        self.addPeerHandler(newMsgLines,client)
    
    def topologyMapperHandler(self, msgLines,client):
        key = msgLines[1]
        if key != self.key:
            msgLines.append(self.key)
            self.send('\n'.join(msgLines),self.successor[1],self.successor[2])
        else:
            self.log.info(self.peerId,'Here is the topology of the network')
            i = 1
            for topoKey in msgLines[1:]:
                self.log.info(self.peerId,'%d, %s'%(i,topoKey))
                i += 1

    def storeFingerHandler(self,msgLines,client=None):
        self.log.info(self.peerId,'Stored new entry (%s,%s,%s)'%(msgLines[1],msgLines[2],msgLines[3]))
        self.fingerTable.addNewEntry((msgLines[1],msgLines[2],int(msgLines[3])))

    def fingerSearchHandler(self,msgLines,client=None):
        try:
            if msgLines[1] in self.values:
                # send found key message to search originator
                self.send('FoundKey\n%s\n%s\n%d\n%s'%(self.key,self.host,self.serverPort,self.values[msgLines[1]]),msgLines[2],int(msgLines[3]))
                # send yes message to requester and)
                self.send('yes',msgLines[4],int(msgLines[5]))
            else:
                # send no to requester
                self.send('no', msgLines[4],int(msgLines[5]))
        except Exception as ex:
            self.log.debug(self.peerId,'Recieved exception %s in fingerSearchHandler'%str(ex))
        
    def fingerSearch(self,entries,msgLines):
        for entry in entries:
            #fingerSearch key search host search port this host and this port
            port = None
            with portsLock:
                port = freePorts.pop(0)
            msg='''
            fingerSearch\n%s\n%s\n%s\n%s\n%d
            '''%(msgLines[1],msgLines[3],msgLines[4],self.host,port)
            self.log.info(self.peerId,'Sending finger search to %s:%d'%(entry[1],entry[2]))
            self.send(msg,entry[1],entry[2])
            # create server and recieve answer from successor
            server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            server.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
            for bindAttempt in range(200):
                try:
                    self.log.debug(self.peerId,'Attempting to bind to port %d'%port)
                    server.bind((self.host,port))
                    break
                except Exception as ex:
                    self.log.debug(self.peerId,'Recieved exception: %s failed to bind to port %d'%(str(ex),port))
                    with portsLock:
                        freePorts.append(port)
                        port = freePorts.pop(0)
            server.listen(1)
            client, addr = server.accept()
            reply = self.recv(client)
            reply = reply.strip()
            server.close()
            with portsLock:
                freePorts.append(port)
            if reply == 'yes':
                return True
        return False
    
    def forwardFingerSearch(self,msgLines):
        successors = self.fingerTable.getSuccessors(msgLines[1])
        if len(successors) == 0:
            return False
        self.log.info(self.peerId,'Found %d successors'%len(successors))
        return self.fingerSearch(successors,msgLines)

    def reverseFingerSearch(self,msgLines):
        predecessors = self.fingerTable.getPredecessors(msgLines[1])
        if len(predecessors) == 0:
            return False
        self.log.info(self.peerId,'Find %d predecessors'%len(predecessors))
        return self.fingerSearch(predecessors,msgLines)

    def forwardSearchHandler(self,msgLines,client=None):
         key = msgLines[1]
         ttl = int(msgLines[2])
         if ttl < 0:
             self.log.debug(self.peerId,'ttl for request for key %s has expired'%(key))
             self.send('ttl for request expired at %s:%d'%(self.host,self.serverPort),msgLines[3],int(msgLines[4]))
             return
         if key in self.values:
             self.log.info(self.peerId,'Found key %s'%key)
             self.send('FoundKey\n%s\n%s\n%d\n%s'%(self.key,self.host,self.serverPort,self.values[key]),msgLines[3],int(msgLines[4]))
             return
         if self.forwardFingerSearch(msgLines) == True:
             return
         self.log.info(self.peerId,'Forwarding search request to successor')
         self.send('forwardSearch\n%s\n%d\n%s\n%s'%(msgLines[1],ttl-1,msgLines[3],msgLines[4]),self.successor[1],self.successor[2])
                                                                                                         
    def reverseSearchHandler(self,msgLines,client=None):
        key = msgLines[1]
        ttl = int(msgLines[2])
        if ttl < 0:
            self.log.debug(self.peerId,'ttl for request for key %s has expired'%(key))
            self.send('ttl for request expired at %s:%d'%(self.host,self.serverPort),msgLines[3],int(msgLines[4]))
            return
        if key in self.values:
            self.log.info(self.peerId,'Found key %s'%key)
            self.send('FoundKey\n%s\n%s\n%d\n%s'%(self.key,self.host,self.serverPort,self.values[key]),msgLines[3],int(msgLines[4]))
            return
        if self.reverseFingerSearch(msgLines) == True:
            return
        self.log.info(self.peerId,'Forwarding search request to predecessor')
        self.send('reverseSearch\n%s\n%d\n%s\n%s'%(msgLines[1],ttl-1,msgLines[3],msgLines[4]),self.predecessor[1],self.predecessor[2])
                                                                                                                                                                  
         
    def searchHandler(self,msgLines,client=None):
        key = msgLines[1]
        if key >= self.key:
            self.forwardSearchHandler(msgLines,client)
        else:
            self.reverseSearchHandler(msgLines,client)              
        

    def addPeerHandler(self,msgLines,client):
        peerKey = msgLines[3]
        succKey = self.successor[0]
        predKey = self.predecessor[0]
        if peerKey == self.key:
            self.log.debug(self.peerId,'Recieved addPeer request with same key as this peer from %s:%d'%(msgLines[1],int(msgLines[2])))
        elif peerKey > self.key and (peerKey < succKey or self.key >= succKey):
            # put between successor and this peer
            self.setSuccessor(msgLines)
        elif peerKey > succKey:
            # forward request to successor
            self.log.debug(self.peerId,'Forwarding request for %s:%d to successor'%(msgLines[1],int(msgLines[2])))
            self.send('\n'.join(msgLines),self.successor[1],self.successor[2])
        elif peerKey < self.key and (peerKey > predKey or self.key <= predKey):
            # put between predecessor and this peer
            self.setPredecessor(msgLines)
        else:
            # forward request to predecessor
            self.log.debug(self.peerId,'Forwarding request for %s:%d to predecessor'%(msgLines[1],int(msgLines[2])))
            self.send('\n'.join(msgLines),self.predecessor[1],self.predecessor[2])
                
    def storeHandler(self,msgLines,client):
        key = msgLines[1]
        self.log.info(self.peerId,'Recieved store request for key %s'%(key))
        if  self.key >= key and key >= self.predecessor[0]: 
            self.values[msgLines[1]] = '\n'.join(msgLines[2:]) # if at correct peer then store data in values array                            
            self.log.debug(self.peerId,'Successfully stored data at peer %d'%(self.peerId))
        elif self.key >= self.successor[0] and key >= self.successor[0]:
            self.values[msgLines[1]] = '\n'.join(msgLines[2:]) # if at correct peer then store data in values array
            self.log.debug(self.peerId,'Successfully stored data at peer %d'%(self.peerId))
        elif self.key <= self.predecessor[0] and key <= self.key:
            self.values[msgLines[1]] = '\n'.join(msgLines[2:]) # if at correct peer then store data in values array
            self.log.debug(self.peerId,'Successfully stored data at peer %d'%(self.peerId))
        elif key > self.key:
            self.log.debug(self.peerId,'Forwarding store request to %s:%d'%(self.successor[1],self.successor[2]))
            self.send('\n'.join(msgLines),self.successor[1],self.successor[2])
        else:
            self.log.debug(self.peerId,'Reversing store request to %s:%d'%(self.predecessor[1],self.predecessor[2]))
            self.send('\n'.join(msgLines),self.predecessor[1],self.predecessor[2])
            
    def defaultHandler(self, msgLines,client):
        self.log.warn(self.peerId,'Unknown request %s recieved'%msgLines[0])                                
        
    def setSuccessorHandler(self,msgLines,client):
        self.successor = (msgLines[3],msgLines[1], int(msgLines[2]))
        self.log.info(self.peerId,'successor is now %s:%d'%(msgLines[1],int(msgLines[2])))
        
    def setPredecessorHandler(self,msgLines,client):
        self.predecessor = (msgLines[3],msgLines[1],int(msgLines[2]))
        self.log.info(self.peerId,'predecessor is now %s:%d'%(msgLines[1],int(msgLines[2])))
    
    def setSuccessor(self,msgLines):
        host = ''
        port = 0
        request = ''
        try:
            # tell successor that it's new predecessor is the new peer
            host = self.successor[1]
            port = self.successor[2]
            request = 'setPredecessor'
            self.log.debug(self.peerId,'Sending setPredecessor request to %s:%d'%(self.successor[1],self.successor[2]))
            self.send('setPredecessor\n%s\n%s\n%s'%(msgLines[1],msgLines[2],msgLines[3]),self.successor[1],self.successor[2])
            self.log.info(self.peerId,'setPredecessor request successfully sent to %s:%d'%(self.successor[1],self.successor[2]))
            #tell new peer who it's successor is
            host = msgLines[1]
            port = int(msgLines[2])
            request = 'setSuccessor'                         
            self.log.debug(self.peerId,'Sending setSuccessor request to %s:%d'%(msgLines[1],int(msgLines[2])))
            self.send('setSuccessor\n%s\n%s\n%s'%(self.successor[1],self.successor[2],self.successor[0]),msgLines[1],int(msgLines[2]))
            self.log.info(self.peerId,'setSuccessor request successfully sent to %s:%d'%(msgLines[1],int(msgLines[2])))
            # tell new peer that you are it's predecessor
            host = msgLines[1]
            port = int(msgLines[2])
            request = 'setPredecessor'                        
            self.log.debug(self.peerId,'Sending setPredecessor request to %s:%d'%(msgLines[1],int(msgLines[2])))
            self.send('setPredecessor\n%s\n%s\n%s'%(self.host,self.serverPort,self.key),msgLines[1],int(msgLines[2]))
            self.log.info(self.peerId,'setPredecessor request successfully sent to %s:%d'%(msgLines[1],int(msgLines[2])))
            # set new successor
            self.successor = (msgLines[3],msgLines[1],int(msgLines[2]))
            self.log.info(self.peerId,'successor is now %s:%d'%(msgLines[1],int(msgLines[2])))
        except Exception as ex:
            self.log.error(self.peerId,'Recieved following error in setSuccessor %s'%str(ex))
            self.log.error(self.peerId,'%s request sent to %s:%d failed to be sent'%(request,host,port))
            raise ex
            
    def setPredecessor(self,msgLines):
        host = ''
        port = 0
        request = ''
        try:
            #tell predecessor that it's new successor is the new peer
            host = self.predecessor[1]
            port = self.predecessor[2]
            request = 'setSuccessor' 
            self.log.debug(self.peerId,'Sending setSuccessor request to %s:%d'%(self.predecessor[1],self.predecessor[2]))
            self.send('setSuccessor\n%s\n%s\n%s'%(msgLines[1],msgLines[2],msgLines[3]),self.predecessor[1],self.predecessor[2])
            self.log.info(self.peerId,'setSuccessor request sent to %s:%d successfully sent'%(self.predecessor[1],self.predecessor[2]))
            # tell new peer who it's predecessor is
            host = msgLines[1]
            port = int(msgLines[2])
            request = 'setPredecessor'
            self.log.debug(self.peerId,'Sending setPredecessor request to %s:%d'%(msgLines[1],int(msgLines[2])))
            self.send('setPredecessor\n%s\n%s\n%s'%(self.predecessor[1],self.predecessor[2],self.predecessor[0]),msgLines[1],int(msgLines[2]))
            self.log.info(self.peerId,'setPredecessor request successfully sent to %s:%d'%(msgLines[1],int(msgLines[2])))
            # tell new peer that you are it's successor
            host = msgLines[1]
            port = int(msgLines[2])
            request = 'setSuccessor'
            self.log.debug(self.peerId,'Sending setSuccessor request to %s:%d'%(msgLines[1],int(msgLines[2])))            
            self.send('setSuccessor\n%s\n%s\n%s'%(self.host,self.serverPort,self.key),msgLines[1],int(msgLines[2]))
            self.log.info(self.peerId,'setSuccessor request successfully sent to %s:%d'%(msgLines[1],int(msgLines[2])))            
            # set new predecessor
            self.predecessor = (msgLines[3],msgLines[1],int(msgLines[2]))
            self.log.info(self.peerId,'predecessor is now %s:%d'%(msgLines[1],int(msgLines[2])))
        except Exception as ex:
            self.log.error(self.peerId,'Recieved following error in setPredecessor %s'%str(ex))
            self.log.error(self.peerId,'%s request sent to %s:%d failed to be sent'%(request,host,port))
            raise ex
    
    def joinSwarm(self,host, port):
        # sends request to (host, port) in order
        # to be added to the p2p network
        msgSent = False
        try: 
            self.log.debug(self.peerId,'Join request being sent to %s:%d'%(host,port))
            # create message to be sent
            msg = 'Join\n%s\n%d\n%s'%(self.host,self.serverPort,self.key)
            self.send(msg,host,port)
            self.log.info(self.peerId,'Join request successfully sent to %s:%d'% (host,port))
            msgSent  = True
        except Exception as ex:
            self.log.error(self.peerId,'Join request to %s:%d failed to be sent'%(host,port))
        return msgSent
    
    def leave(self,msgLines):
        pass

    def send(self,msg,host,port,client=None):
        msgLen = len(msg)
        totalSent = 0
        self.lock.acquire()
        for i in range(200):
            try:
                if client != None:
                    self.client = client
                else:
                    self.client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
                    self.client.connect((host,port))
                    self.client.settimeout(10)
                while totalSent < msgLen:
                    sent = self.client.send(msg[totalSent:])
                    if sent == 0:
                        self.lock.release()
                        raise RuntimeError('Socket connection broken')
                    totalSent += sent
                if client == None:
                    self.client.close()
                self.lock.release()
                return
            except Exception:
                self.log.debug(self.peerId,'Failed to send message retrying for %d time'%i)
        self.lock.release()
        raise Exception('Could not send message')
            
    def recv(self,client):
        msg = ''
        while True:
            data = client.recv(1024)
            if not data:
                break
            else:
                msg += data
        return msg.strip()

    def serverBind(self):
        try:
            self.server.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1) # allows to reuse port address
            self.server.bind((self.host,self.serverPort))
            return True
        except socket.error as se:
            self.log.error(self.peerId,'Binding server socket to port %d failed for host %s'% \
                           (self.serverPort,self.host))
            return False
            
    def runServer(self):
        self.server.listen(100)
        while True:
            client, addr = self.server.accept()
            try:
                msg = self.recv(client)
                msgLines = msg.split('\n')
                #self.handlers.get(msgLines[0],self.defaultHandler)(msgLines,client)
                handler = Thread(target=self.handlers.get(msgLines[0],self.defaultHandler),args=(msgLines,client,))
                handler.start()
            except socket.timeout:
                self.log.error(self.peerId,'Recieved timeout exception')
            except Exception as ex:
                self.log.error(self.peerId,'Recieved an unexpected error: %s' % (ex))
                traceback.print_exc(file=sys.stdout)
            finally:
                client.close()

    def run(self):
        self.runServer()
