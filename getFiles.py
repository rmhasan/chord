import sys, os, socket, time
from misc import *

def search(inputDir,outputDir,peerIp,hostIp,ttl):
    execTimes = []
    for _,_,files in os.walk(inputDir):
        for filename in files:
            if not filename.endswith('.key'):
                continue
            keys = open(inputDir+'/'+filename,'r').readlines()
            content = ''
            for key in keys:
                key = key.strip()
                try:
                    send('search\n%s\n%s\n%s\n%d'%(key,ttl,hostIp,49999),peerIp,6000)
                    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
                    server.bind(('',49999))
                    server.listen(1)
                    t1 = time.clock()
                    client, addr = server.accept()
                    reply  = recv(client)
                    reply = reply.strip()
                    repComps = reply.split('\n')
                    if repComps[0] == 'FoundKey':
                        print 'Found key %s at %s:%s'%(key,repComps[2],repComps[3])
                        execTimes.append((time.clock() - t1) * 1000)
                        send('storeFinger\n%s\n%s\n%s'%(repComps[1],repComps[2],repComps[3]),peerIp,6000)
                        content += '\n'.join(repComps[5:])
                    else:
                        execTimes.append(-1)
                        print reply
                    server.close()
                except Exception as ex:
                    print ex
            outObj = open(outputDir+'/'+'.'.join(filename.split('.')[:-1]),'w+')
            outObj.write(content)
            outObj.close()
    return execTimes
                
if len(sys.argv) != 6:
    print 'usage getFiles.py <input dir> <output dir> <peerIp> <hostIp> <ttl>'
    sys.exit(0)

results = []
for i in range(2):
    results.append(search(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5]))

resObj = open('results/results_%s.out'%sys.argv[1],'w+')
for iteration in range(len(results[0])):
    s = ''
    for i in range(1):
        s += str(results[i][iteration])+','
    s += str(results[1][iteration])
    resObj.write(s+'\n')
