#!/usr/bin/python

import sys
import atexit
import time
import hashlib

import mininet.util
import mininext.util
mininet.util.isShellBuiltin = mininext.util.isShellBuiltin
sys.modules['mininet.util'] = mininet.util

from mininet.util import dumpNodeConnections
from mininet.node import OVSController
from mininet.log import setLogLevel, info

from mininext.cli import CLI
from mininext.net import MiniNExT

from topo import QuaggaTopo

net = None


def startNetwork(numHosts,multiple=10,halfCacheLength=2):
    "instantiates a topo, then starts the network and prints debug information"

    info('** Creating Quagga network topology\n')
    topo = QuaggaTopo(numHosts)

    info('** Starting the network\n')
    global net
    net = MiniNExT(topo, controller=OVSController)
    net.start()

    info('** Dumping host connections\n')
    dumpNodeConnections(net.hosts)

    info('** Starting first peers\n')
    firstHost = topo.topoHosts[0]
    info('** Starting peer on %s'%firstHost[0])
    print net[firstHost[0]].cmd('python spawnpeer.py %s 0 0'%firstHost[1])
    time.sleep(3)
    i = 1
    for host in topo.topoHosts[1:]:
        info('** Starting peer on %s'%host[0])
        print net[host[0]].cmd('python spawnpeer.py %s %s %d'%(host[1],firstHost[1],i*multiple))
        i += 1
        time.sleep(3)
    idx = 0
    for host in topo.topoHosts:
        forwardPeers = [(((i+idx) % halfCacheLength) * multiple,topo.topoHosts[(i+idx) % halfCacheLength]) for i in range(halfCacheLength)]
        reversePeers = [(((i-idx) % halfCacheLength) * multiple,topo.topoHosts[(i-idx) % halfCacheLength]) for i in range(halfCacheLength)]
        pidplus = 1
        for fp in forwardPeers:
            if fp[1][1] == host[1]:
                continue
            pid = idx*multiple + pidplus
            key = str(hashlib.sha1(str(pid)).hexdigest())
            info('** Sending store request to %s'%host[1])
            print net[host[0]].cmd('python sendmsg.py "storeFinger\n%s\n%s\n%d" %s'%(key,fp[1][1],6000,host[1]))
            pidplus += 1
        for rp in reversePeers:
            if rp[1][1] == host[1]:
                continue
            pid = idx*multiple + pidplus
            key = str(hashlib.sha1(str(pid)).hexdigest())
            info('** Sending store request to %s'%host[1])
            print net[host[0]].cmd('python sendmsg.py "storeFinger\n%s\n%s\n%d" %s'%(key,rp[1][1],6000,host[1]))
            pidplus += 1
        idx += 1
    info('** Running CLI\n')
    CLI(net)

def stopNetwork():
    "stops a network (only called on a forced cleanup)"

    if net is not None:
        info('** Tearing down Quagga network\n')
        net.stop()

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage: start.py <num hosts> <multiple>'
        sys.exit(0)
    numHosts = int(sys.argv[1])
    multiple = int(sys.argv[2])
    if numHosts > 512:
        print 'Currently does not support past 512 peers'
        sys.exit(0)
    # Force cleanup on exit by registering a cleanup function
    atexit.register(stopNetwork)

    # Tell mininet to print useful information
    setLogLevel('info')
    startNetwork(numHosts,multiple)
