from peer import Peer
from logger import Logger
from random import randint
from args import *

import sys, thread, hashlib, socket,time, timeit

def runServer(peer):
    peer.runServer()
	
def send(msg,host,port,client=None):
     msgLen = len(msg)
     totalSent = 0
     if client == None:
         sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
         sock.connect((host,port))
         sock.settimeout(10)
     else:
         sock = client
     while totalSent < msgLen:
         sent = sock.send(msg[totalSent:])
         if sent == 0:
             raise RuntimeError('Socket connection broken')
         totalSent += sent
     if client == None:     
        sock.close()
                                                                                                             
def recv(client):
    msg = ''
    client.settimeout(10)
    while True:
        data = client.recv(1024)
        if not data:
            break
        msg += (''+data)
    return msg.strip()
                                                                                         
     
def pushData(peers):
    n = len(peers) - 1
    keys = []
    for i in range(0,n*10):
        peer = peers[randint(0,n)]
        key = hashlib.sha1(str(i)).hexdigest()
        msg = 'store\n%s\n%d'%(hashlib.sha1(str(i)).hexdigest(),i)
        send(msg,peer.host,peer.serverPort)
        keys.append(key)
    return keys

def searchData(peers,keys,ttl,log):
    ttl = int(ttl)
    execTimes = []
    for key in keys:
        peer = peers[randint(0,len(peers)-1)]
        try:
            send('%s\n%s\n%d\n%s\n%d'%('search',key,ttl,'localhost',49999),peer.host,peer.serverPort)
            server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            server.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
            server.bind(('',49999))
            server.listen(1)
            t1 = time.clock()
            client, addr = server.accept()
            reply = recv(client)
            reply = reply.strip()
            repComps = reply.split('\n')
            execTime = None
            if repComps[0] == 'FoundKey':
                log.info(-1,'Found key %s at %s:%s'%(key,repComps[2],repComps[3]))
                execTimes.append((time.clock() - t1)*1000)
                # need to test line below
                send('storeFinger\n%s\n%s\n%s'%(repComps[1],repComps[2],repComps[3]),peer.host,peer.serverPort)
            else:
                execTimes.append(-1)
                log.info(-1,reply)
            server.close()
        except Exception as ex:
            print ex
    return execTimes
    
def getTopology(peer):
    msg = 'topology\n%s'%peer.key
    send(msg,peer.successor[1],peer.successor[2])
        
def main():
	peerId = 0
	port = 6000
	args = parseArgs(sys.argv[1:])
	
	logArgs = {'outFile':args['fileName'], 'outDir':'Output', 'level':'DEBUG', 'timestamp':'True', 'console':args['console']}
	log = Logger(**logArgs)
		
	firstPeer = Peer(peerId,'127.0.0.1',port,log)
	peers = [firstPeer]
	thread.start_new_thread(runServer,(firstPeer,))
	for i in range(args['numPeers']):
		peerId += 1
		port += 1
		newPeer = Peer(peerId * 10,'127.0.0.1',port,log)
		thread.start_new_thread(runServer,(newPeer,))
		newPeer.joinSwarm(firstPeer.host,firstPeer.serverPort)
		peers.append(newPeer)
		time.sleep(3)
        getTopology(firstPeer)
		time.sleep(5)
        print '\nStoring data\n'
        keys = pushData(peers)
		time.sleep(5)
        print '\nSearching for data\n'
        execTimes = searchData(peers,keys,args['numPeers'],log)
		time.sleep(5)
        for et in execTimes:
            print et
		while True:
			pass

class Swarm:

	def __init__(self, numPeers, biDirectional, initialized):
		self.numPeers = numPeers
		self.biDirectional = biDirectional
		self.initialized = initialized
		
		
		logArgs = {'level':'NULL', 'console':False}
		self.log = Logger(**logArgs)
		
		# If initialized is True, then we want to initialize the various finger tables
		# We also want to create the various peers before we start doing searches. 
		# Remember, that is not what we want to be measuring, we ONLY want to measure search time
		# 
		# Logger here is set to NULL. We DO NOT want the logger to run when we are getting metrics, that 
		# will have a confounding effect.

	def initialize():
		return
		
		
	def test():
		peerId = 0
		port = 6000
		
		# Most of this should be moved outside of test(), maybe into initialize()
		firstPeer = Peer(peerId,'127.0.0.1', port, self.log)
		peers = [firstPeer]
		thread.start_new_thread(runServer,(firstPeer,))
		for i in range(self.numPeers):
			peerId += 1
			port += 1
			newPeer = Peer(peerId * 10,'127.0.0.1', port, self.log)
			thread.start_new_thread(runServer,(newPeer,))
			newPeer.joinSwarm(firstPeer.host, firstPeer.serverPort)
			peers.append(newPeer)
			getTopology(firstPeer)
			keys = pushData(peers)
			execTimes = searchData(peers, keys, self.numPeers, self.log)
		
		
if __name__ == '__main__':
	main()
	

