import sys, os, hashlib
from misc import *

CHUNK_SIZE = 512

def pushFiles(directory, peerIp):
    for _, _, files in os.walk(directory):
        for filename in files:
            if filename.endswith('.key'):
                continue
            if not os.path.isfile(directory+'/'+filename):
                continue
            fobj = open(directory+'/'+filename,'r')
            content = fobj.read()
            chunks = [content[i:i+512] for i in range(0,len(content),512)]
            fileKeys = []
            print 'Sending %d chunks of file %s to host %s for storage'%(len(chunks),filename,peerIp)
            for chunk in chunks:
                data = filename + '\n' + chunk
                key = str(hashlib.sha1(data).hexdigest())
                msg = 'store\n%s\n%s'%(key,data)
                send(msg,peerIp,6000)
                fileKeys.append(key)
                outObj = open(directory+'/'+filename+'.key','w+')
                for key in fileKeys:
                    outObj.write(key+'\n')

if len(sys.argv) != 3:
    print 'usage: uploadFiles.py <directory> <peerIp>'
    sys.exit(0)

pushFiles(sys.argv[1],sys.argv[2])




